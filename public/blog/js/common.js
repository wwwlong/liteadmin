$(function () {
    $(document).on('click','a[target!="_blank"]',function (ev) {
        if(history.pushState){
            ev.preventDefault();
            var url = $(ev.target).attr('href');
            if (url === location.pathname){
                return;
            }

            $.ajax({
                url:url,
                method:'get'
            }).done(function (data) {
                var html = $(data).find('#pjax-container').html();
                var title = $(data).filter('title').text();
                $('#pjax-container').html(html);
                document.title = title;
                window.history.pushState(200, title, url);
            })
        }
    })
})