<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/4/22 0022
 * Time: 下午 21:24
 */

namespace app\common\command;

use jay\Surport\Backup;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\facade\Config;
use think\facade\Env;

/**
 * 数据库备份
 * Class DataBackup
 * @package app\common\command
 */
class DataBackup extends Command
{
    protected function configure(){
        $this->setName("databackup")->setDescription("database backup");
    }
    
    protected function execute(Input $input, Output $output){
        $database = Config::pull("database");
        $dbconf = [
            'username' => $database['username'],
            'password' => $database['password'],
            'host' => $database['hostname'].":".$database['hostport'],
            'database' => $database['database'],
        ];
        $db = app('databackup',['database'=>$dbconf]);
        $db->setWithData(true)->setFilename('liteadmin')->dump();
        $output->writeln("Database has been backup!");
    }
}