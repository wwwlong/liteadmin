<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/9/29
 * Time: 22:22
 */

namespace app\common\model;

use think\Model;

class Tags extends Model
{
    protected $table = "content_tags";
}