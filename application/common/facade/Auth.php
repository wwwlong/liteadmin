<?php
/**
 * https://gitee.com/Mao02
 * http://www.mao02.win/
 * jay_fun 410136330@qq.com
 * Date: 2018/4/6 0006
 * Time: 上午 0:07
 */

namespace app\common\facade;

use think\Facade;

/**
 * @see \app\common\service\Auth
 * @mixin \app\common\service\Auth
 * @method bool auth(string $path) static 判断当前$path是否有访问权限
 * @method mixed getAllAcess(string $prefix = '') static 获取当前用户全部权限 节点ID
 */
class Auth extends Facade
{
    protected static function getFacadeClass()
    {
        return \app\common\service\Auth::class;
    }
}