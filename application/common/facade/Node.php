<?php
/**
 * https://gitee.com/Mao02
 * http://www.mao02.win/
 * jay_fun 410136330@qq.com
 * Date: 2018/4/6 0006
 * Time: 上午 0:07
 */
namespace app\common\facade;

use think\Facade;

/**
 * @see \app\common\service\Node
 * @mixin \app\common\service\Node
 * @method void reload() static 刷新数据库节点数据
 * @method array getFileNodes() static 扫描文件系统 获取全部节点
 * @method array getNodesData() static 从数据库中获取节点缓存
 */
class Node extends Facade
{
    protected static function getFacadeClass()
    {
        return \app\common\service\Node::class;
    }
}