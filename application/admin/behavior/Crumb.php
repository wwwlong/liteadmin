<?php
/**
 * https://gitee.com/Mao02
 * http://www.mao02.win/
 * jay_fun 410136330@qq.com
 * Date: 2018/4/5 0005
 * Time: 下午 18:41
 */

namespace app\admin\behavior;

use think\Db;
use think\facade\Request;
use think\facade\View;

/**
 * 面包屑行为
 * Class Crumb
 * @package app\admin\behavior
 */
class Crumb {

	public function run() {
		$module = Request::module();
		$controller = Request::controller();
		$action = Request::action();

		$ctitie = Db::name('SystemAuthNode')->where('path',"{$module}/{$controller}")->value('title');
		$atitie = Db::name('SystemAuthNode')->where('path',"{$module}/{$controller}/{$action}")->value('title');

		View::assign(['ctitle'=>$ctitie,'atitle'=>$atitie]);
	}
}