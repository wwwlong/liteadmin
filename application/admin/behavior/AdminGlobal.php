<?php
/**
 * https://gitee.com/Mao02
 * http://www.mao02.win/
 * jay_fun 410136330@qq.com
 * Date: 2018/4/6 0006
 * Time: 上午 1:00
 */

namespace app\admin\behavior;
use think\facade\Request;
use think\facade\View;

/**
 * 后台全局
 * Class AdminGlobal
 * @package app\admin\behavior
 */
class AdminGlobal {
	/**
	 * 执行入口
	 */
	public function run() {
        $classuri = Request::module().'/'.Request::controller();
		View::assign('classuri',$classuri);
	}
}