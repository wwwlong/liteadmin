<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/9/25
 * Time: 22:47
 */

namespace app\admin\controller;

use app\common\controller\BasicAdmin;
use think\Db;
use think\Request;

/**
 * @title 标签管理
 * Class Tags
 * @package app\admin\controller
 */
class Tags extends BasicAdmin
{
    protected $table = 'content_tags';

    /**
     * @title 列表页
     * @auth 2
     * @param Request $request
     * @return array|mixed|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index(Request $request)
    {
        $db = Db::name($this->table)
            ->alias('t')
            ->join('__CONTENT_TAGS_MAP__ m','m.tag_id = t.id','LEFT')
            ->field('t.*,count(m.tag_id) as article_count')
            ->group('m.tag_id');

        return $this->_list($db);
    }

    /**
     * @title 删除
     * @throws PDOException
     * @throws \think\Exception
     */
    public function del(Request $request)
    {
        $ids = $request->get('ids');
        $hasArt = Db::name('content_tags_map')->whereIn('tag_id',$ids)->count();
        if ($hasArt){
            $this->error("删除的标签下还有文章，未给予删除");
        }
        $this->_del($ids);
    }
}