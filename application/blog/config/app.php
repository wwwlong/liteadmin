<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/9/29
 * Time: 20:16
 */
return [
    'app_trace' => false,
    'app_debug' => false,

    'default_ajax_return'    => 'html',
];