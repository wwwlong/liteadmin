<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/10/7 0007
 * Time: 12:30
 */
/**
 * 带缓存的网站配置获取
 * @param string $name
 * @return array|string
 */
function get_site_config($name=''){
    static $config;
    if (empty($config)){
        $config = \think\Db::name('site_config')->column('value','name');
    }
    if (empty($name)){
        return $config;
    }
    return $config[$name]?:'';
}