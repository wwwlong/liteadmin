<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/9/29
 * Time: 20:39
 */

namespace app\blog\controller;

use think\Controller;
use think\Request;
use app\common\model\Link as LinkModel;


class Link extends Controller
{
    public function index(Request $request)
    {
        $links = LinkModel::all(['state'=>1]);
        $this->assign('links',$links);
        return $this->fetch();
    }
}