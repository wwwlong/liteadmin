<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/5/25
 * Time: 14:13
 */

return [
    'auth'=>\app\common\service\Auth::class,
    'node'=>\app\common\service\Node::class,
    'tree'=>\app\common\service\Tree::class,
    'databackup'=>\app\common\service\Backup::class,
];