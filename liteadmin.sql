-- --------------------------------------------------
--      备份分工具
--      www.mao02.win    
--      猫铃儿提供技术支持     
-- --------------------------------------------------
--          数据表 content_ads
-- --------------------------------------------------

DROP TABLE IF EXISTS `content_ads`;

CREATE TABLE IF NOT EXISTS `content_ads` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL COMMENT '所属分类',
  `title` varchar(255) NOT NULL COMMENT '名称',
  `url` varchar(255) NOT NULL COMMENT '链接',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `image` varchar(255) NOT NULL COMMENT '图片',
  `state` int(1) NOT NULL DEFAULT '1' COMMENT '1 启用',
  `create_time` int(11) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0' COMMENT '1 已删',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- --------------------------------------------------
--          数据表 content_adscate
-- --------------------------------------------------

DROP TABLE IF EXISTS `content_adscate`;

CREATE TABLE IF NOT EXISTS `content_adscate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '分类名称',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `state` int(255) NOT NULL DEFAULT '0' COMMENT '0 禁用',
  `is_deleted` int(1) NOT NULL COMMENT '1 已删',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- --------------------------------------------------
--          数据表 content_article
-- --------------------------------------------------

DROP TABLE IF EXISTS `content_article`;

CREATE TABLE IF NOT EXISTS `content_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `md_content` longtext NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `is_deleted` tinyint(1) unsigned NOT NULL,
  `state` tinyint(1) unsigned NOT NULL COMMENT '显示隐藏',
  `click` int(11) NOT NULL COMMENT '点击次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

INSERT INTO `content_article` VALUES('1','4','linux系统的关机命令','linux，命令行','这里介绍几种常用的linux系统关机命令','','<ul>\n<li>halt</li>\n<li>init 0</li>\n<li>shutdown -h now</li>\n</ul>','- halt\r\n- init 0\r\n- shutdown -h now\r\n\r\n','1537929187','1537931249','0','1','5');
INSERT INTO `content_article` VALUES('2','5','爬虫需谨慎，你不知道的爬虫与反爬虫套路！','爬虫','爬虫与反爬虫，是一个很不阳光的行业。这里说的不阳光，有两个含义。','','<p>第一是，这个行业是隐藏在地下的，一般很少被曝光出来。很多公司对外都不会宣称自己有爬虫团队，甚至隐瞒自己有反爬虫团队的事实。这可能是出于公司战略角度来看的，与技术无关。</p>\n<p>第二是，这个行业并不是一个很积极向上的行业。很多人在这个行业摸爬滚打了多年，积攒了大量的经验，但是悲哀的发现，这些经验很难兑换成闪光的简历。</p>\n<p>面试的时候，因为双方爬虫理念或者反爬虫理念不同，也很可能互不认可，影响自己的求职之路。本来程序员就有“文人相轻”的倾向，何况理念真的大不同。</p>\n<p>然而这就是程序员的宿命。不管这个行业有多么的不阳光，依然无法阻挡大量的人进入这个行业，因为有公司的需求。</p>','第一是，这个行业是隐藏在地下的，一般很少被曝光出来。很多公司对外都不会宣称自己有爬虫团队，甚至隐瞒自己有反爬虫团队的事实。这可能是出于公司战略角度来看的，与技术无关。\r\n\r\n第二是，这个行业并不是一个很积极向上的行业。很多人在这个行业摸爬滚打了多年，积攒了大量的经验，但是悲哀的发现，这些经验很难兑换成闪光的简历。\r\n\r\n面试的时候，因为双方爬虫理念或者反爬虫理念不同，也很可能互不认可，影响自己的求职之路。本来程序员就有“文人相轻”的倾向，何况理念真的大不同。\r\n\r\n然而这就是程序员的宿命。不管这个行业有多么的不阳光，依然无法阻挡大量的人进入这个行业，因为有公司的需求。\r\n\r\n','1537929392','1537931502','0','1','1');
INSERT INTO `content_article` VALUES('3','0','Swoole：面向生产环境的 PHP 异步网络通信引擎','swoole','面向生产环境的 PHP 异步网络通信引擎','/uploads/20180926/ab4669ac2af7cbc0ede45fd0a14a20e0.png','<p>使 PHP 开发人员可以编写高性能的异步并发 TCP、UDP、Unix Socket、HTTP，WebSocket 服务。Swoole 可以广泛应用于互联网、移动通信、企业软件、云计算、网络游戏、物联网（IOT）、车联网、智能家居等领域。 使用 PHP + Swoole 作为网络通信框架，可以使企业 IT 研发团队的效率大大提升，更加专注于开发创新产品。</p>','使 PHP 开发人员可以编写高性能的异步并发 TCP、UDP、Unix Socket、HTTP，WebSocket 服务。Swoole 可以广泛应用于互联网、移动通信、企业软件、云计算、网络游戏、物联网（IOT）、车联网、智能家居等领域。 使用 PHP + Swoole 作为网络通信框架，可以使企业 IT 研发团队的效率大大提升，更加专注于开发创新产品。','1537929422','1537931687','0','1','2');


-- --------------------------------------------------
--          数据表 content_attachment
-- --------------------------------------------------

DROP TABLE IF EXISTS `content_attachment`;

CREATE TABLE IF NOT EXISTS `content_attachment` (
  `hash` char(32) NOT NULL,
  `path` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `size` int(11) unsigned NOT NULL,
  PRIMARY KEY (`hash`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

INSERT INTO `content_attachment` VALUES('06cde5906b787a58200dd8c13bbffb28','/uploads/20180926/ab4669ac2af7cbc0ede45fd0a14a20e0.png','1537931649','32492');
INSERT INTO `content_attachment` VALUES('37bcfade3025ef31b13827b005c69704','/uploads/20180814/b1c4adacbc7289868fa33d071e6a455b.png','1534240210','190184');
INSERT INTO `content_attachment` VALUES('3c8961c92429c5a92cc44ae2b7dfce01','/uploads/20180814/43a5f5c1010b4811aeebe2ca247bfac4.png','1534240151','1616');
INSERT INTO `content_attachment` VALUES('64d37eca7d4ec3c70ba27103a77f32ff','/uploads/20180814/693db0bd37e80801bd431449ad112ae6.jpg','1534240160','152586');
INSERT INTO `content_attachment` VALUES('72bae0474d2106c1642b03aaaa492d82','/uploads/20180926/24dac561d3c452e04980d5a4c7fe65b9.jpg','1537930751','2535');
INSERT INTO `content_attachment` VALUES('fe134da6f0c7fac92184a0db36ce834c','/uploads/20180814/3a424041ca5c6d0c5128df1901eceefb.png','1534239731','31503');


-- --------------------------------------------------
--          数据表 content_category
-- --------------------------------------------------

DROP TABLE IF EXISTS `content_category`;

CREATE TABLE IF NOT EXISTS `content_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `seo_keyword` varchar(255) NOT NULL,
  `seo_description` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `theme` varchar(255) NOT NULL COMMENT '模板',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `state` tinyint(1) unsigned NOT NULL COMMENT '显示隐藏',
  `content` text NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

INSERT INTO `content_category` VALUES('4','0','linux','linux','linux系统学习','0,4','index','0','1','<p><img src=\"/uploads/20180926/24dac561d3c452e04980d5a4c7fe65b9.jpg\" alt=\"24dac561d3c452e04980d5a4c7fe65b9.jpg\" /><br /></p>');
INSERT INTO `content_category` VALUES('5','0','后端开发','后端代码开发','后端代码开发','0,5','index','0','1','<p><br /></p>');
INSERT INTO `content_category` VALUES('6','5','PHP','PHP语言开发','PHP语言开发','0,5,6','index','0','1','<p>PHP语言开发</p>');


-- --------------------------------------------------
--          数据表 content_link
-- --------------------------------------------------

DROP TABLE IF EXISTS `content_link`;

CREATE TABLE IF NOT EXISTS `content_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `href` varchar(255) NOT NULL,
  `state` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `content_link` VALUES('2','我的博客','http://blog.dazhetu.cn','1');


-- --------------------------------------------------
--          数据表 content_tags
-- --------------------------------------------------

DROP TABLE IF EXISTS `content_tags`;

CREATE TABLE IF NOT EXISTS `content_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `content_tags` VALUES('3','linux');
INSERT INTO `content_tags` VALUES('4','命令行');
INSERT INTO `content_tags` VALUES('5','爬虫');
INSERT INTO `content_tags` VALUES('6','swoole');


-- --------------------------------------------------
--          数据表 content_tags_map
-- --------------------------------------------------

DROP TABLE IF EXISTS `content_tags_map`;

CREATE TABLE IF NOT EXISTS `content_tags_map` (
  `tag_id` int(10) NOT NULL,
  `article_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `content_tags_map` VALUES('3','1');
INSERT INTO `content_tags_map` VALUES('4','1');
INSERT INTO `content_tags_map` VALUES('5','2');
INSERT INTO `content_tags_map` VALUES('6','3');


-- --------------------------------------------------
--          数据表 site_config
-- --------------------------------------------------

DROP TABLE IF EXISTS `site_config`;

CREATE TABLE IF NOT EXISTS `site_config` (
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `site_config` VALUES('site_name','我的网站','网站名称');


-- --------------------------------------------------
--          数据表 system_admin
-- --------------------------------------------------

DROP TABLE IF EXISTS `system_admin`;

CREATE TABLE IF NOT EXISTS `system_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `name` varchar(10) NOT NULL COMMENT '姓名',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  `last_login` int(11) unsigned NOT NULL COMMENT '上次登录时间',
  `is_deleted` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `system_admin` VALUES('1','admin','$2y$11$iR/m.ctNLiS/0g9SkPDE.OElmV6pTP.En79wF7JgQLQt2RXUNjiwa','超级管理员','0','1538229981','0','1');
INSERT INTO `system_admin` VALUES('2','bianji','$2y$11$8jV2bGk6xqBvmsTWXfZ2IO9ij0yaB3YDxirv.56ZlNmfaO8wYs8vK','测试编辑','1527521448','1538139362','0','0');


-- --------------------------------------------------
--          数据表 system_auth_map
-- --------------------------------------------------

DROP TABLE IF EXISTS `system_auth_map`;

CREATE TABLE IF NOT EXISTS `system_auth_map` (
  `admin_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `system_auth_map` VALUES('1','2');
INSERT INTO `system_auth_map` VALUES('1','3');
INSERT INTO `system_auth_map` VALUES('2','3');


-- --------------------------------------------------
--          数据表 system_auth_node
-- --------------------------------------------------

DROP TABLE IF EXISTS `system_auth_node`;

CREATE TABLE IF NOT EXISTS `system_auth_node` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL DEFAULT '',
  `path` varchar(255) NOT NULL,
  `auth` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0不限制 1登录 2授权',
  `level` int(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

INSERT INTO `system_auth_node` VALUES('28','admin','admin','0','1');
INSERT INTO `system_auth_node` VALUES('29','首页','admin/Index','0','2');
INSERT INTO `system_auth_node` VALUES('30','首页','admin/Index/index','0','3');
INSERT INTO `system_auth_node` VALUES('31','欢迎页','admin/Index/welcome','0','3');
INSERT INTO `system_auth_node` VALUES('32','菜单管理','admin/Menu','0','2');
INSERT INTO `system_auth_node` VALUES('33','列表页','admin/Menu/index','2','3');
INSERT INTO `system_auth_node` VALUES('34','权限节点','admin/Node','0','2');
INSERT INTO `system_auth_node` VALUES('35','列表页','admin/Node/index','2','3');
INSERT INTO `system_auth_node` VALUES('37','刷新权限数据','admin/Node/clear','2','3');
INSERT INTO `system_auth_node` VALUES('41','添加','admin/Menu/add','2','3');
INSERT INTO `system_auth_node` VALUES('43','编辑','admin/Menu/edit','2','3');
INSERT INTO `system_auth_node` VALUES('44','删除菜单','admin/Menu/del','2','3');
INSERT INTO `system_auth_node` VALUES('45','用户角色管理','admin/Role','0','2');
INSERT INTO `system_auth_node` VALUES('46','列表页','admin/Role/index','2','3');
INSERT INTO `system_auth_node` VALUES('47','添加','admin/Role/add','2','3');
INSERT INTO `system_auth_node` VALUES('48','编辑','admin/Role/edit','2','3');
INSERT INTO `system_auth_node` VALUES('49','授权','admin/Role/access','2','3');
INSERT INTO `system_auth_node` VALUES('50','删除','admin/Role/del','2','3');
INSERT INTO `system_auth_node` VALUES('51','后台管理员','admin/Admin','0','2');
INSERT INTO `system_auth_node` VALUES('52','列表页','admin/Admin/index','2','3');
INSERT INTO `system_auth_node` VALUES('53','添加','admin/Admin/add','2','3');
INSERT INTO `system_auth_node` VALUES('54','编辑','admin/Admin/edit','2','3');
INSERT INTO `system_auth_node` VALUES('55','删除','admin/Admin/del','2','3');
INSERT INTO `system_auth_node` VALUES('56','重置密码','admin/Admin/password','2','3');
INSERT INTO `system_auth_node` VALUES('57','授予角色','admin/Admin/role','2','3');
INSERT INTO `system_auth_node` VALUES('58','禁用/启用操作','admin/Admin/change','2','3');
INSERT INTO `system_auth_node` VALUES('60','登录控制器','admin/Login','0','2');
INSERT INTO `system_auth_node` VALUES('61','登录操作','admin/Login/login','0','3');
INSERT INTO `system_auth_node` VALUES('65','分类管理','admin/Category','0','2');
INSERT INTO `system_auth_node` VALUES('66','列表页','admin/Category/index','2','3');
INSERT INTO `system_auth_node` VALUES('67','添加','admin/Category/add','2','3');
INSERT INTO `system_auth_node` VALUES('68','编辑','admin/Category/edit','2','3');
INSERT INTO `system_auth_node` VALUES('69','退出登录','admin/Login/logout','1','3');
INSERT INTO `system_auth_node` VALUES('70','文章管理','admin/Article','0','2');
INSERT INTO `system_auth_node` VALUES('71','列表页','admin/Article/index','2','3');
INSERT INTO `system_auth_node` VALUES('72','添加','admin/Article/add','2','3');
INSERT INTO `system_auth_node` VALUES('73','编辑','admin/Article/edit','2','3');
INSERT INTO `system_auth_node` VALUES('74','删除','admin/Article/del','2','3');
INSERT INTO `system_auth_node` VALUES('75','禁用/启用操作','admin/Article/change','2','3');
INSERT INTO `system_auth_node` VALUES('76','删除','admin/Category/del','2','3');
INSERT INTO `system_auth_node` VALUES('77','禁用/启用操作','admin/Category/change','2','3');
INSERT INTO `system_auth_node` VALUES('78','友情链接','admin/Link','0','2');
INSERT INTO `system_auth_node` VALUES('79','列表页','admin/Link/index','2','3');
INSERT INTO `system_auth_node` VALUES('80','添加','admin/Link/add','2','3');
INSERT INTO `system_auth_node` VALUES('81','编辑','admin/Link/edit','2','3');
INSERT INTO `system_auth_node` VALUES('82','删除','admin/Link/del','2','3');
INSERT INTO `system_auth_node` VALUES('83','禁用/启用操作','admin/Link/change','2','3');
INSERT INTO `system_auth_node` VALUES('84','上传','admin/Upload','0','2');
INSERT INTO `system_auth_node` VALUES('85','文件上传','admin/Upload/file','1','3');
INSERT INTO `system_auth_node` VALUES('86','检查图片是否存在','admin/Upload/checkFile','1','3');
INSERT INTO `system_auth_node` VALUES('87','百度umeditor富文本编辑器图片插入','admin/Upload/ueditor','1','3');
INSERT INTO `system_auth_node` VALUES('88','广告内容管理','admin/Ads','0','2');
INSERT INTO `system_auth_node` VALUES('89','列表页','admin/Ads/index','2','3');
INSERT INTO `system_auth_node` VALUES('90','添加','admin/Ads/add','2','3');
INSERT INTO `system_auth_node` VALUES('91','编辑','admin/Ads/edit','2','3');
INSERT INTO `system_auth_node` VALUES('92','删除','admin/Ads/del','2','3');
INSERT INTO `system_auth_node` VALUES('93','禁用/启用操作','admin/Ads/change','2','3');
INSERT INTO `system_auth_node` VALUES('94','广告分类管理','admin/Adscate','0','2');
INSERT INTO `system_auth_node` VALUES('95','列表页','admin/Adscate/index','2','3');
INSERT INTO `system_auth_node` VALUES('96','添加','admin/Adscate/add','2','3');
INSERT INTO `system_auth_node` VALUES('97','编辑','admin/Adscate/edit','2','3');
INSERT INTO `system_auth_node` VALUES('98','删除','admin/Adscate/del','2','3');
INSERT INTO `system_auth_node` VALUES('99','禁用/启用操作','admin/Adscate/change','2','3');
INSERT INTO `system_auth_node` VALUES('100','站点配置信息','admin/Config','0','2');
INSERT INTO `system_auth_node` VALUES('101','设置站点','admin/Config/edit','2','3');
INSERT INTO `system_auth_node` VALUES('102','wangEditor富文本编辑器图片插入','admin/Upload/wangeditor','1','3');
INSERT INTO `system_auth_node` VALUES('103','标签管理','admin/Tags','0','2');
INSERT INTO `system_auth_node` VALUES('104','列表页','admin/Tags/index','2','3');
INSERT INTO `system_auth_node` VALUES('105','删除','admin/Tags/del','2','3');
INSERT INTO `system_auth_node` VALUES('106','markdown编辑器图片插入','admin/Upload/markdown','1','3');
INSERT INTO `system_auth_node` VALUES('107','编辑','admin/Index/edit','1','3');
INSERT INTO `system_auth_node` VALUES('108','修改我的密码','admin/Index/password','1','3');


-- --------------------------------------------------
--          数据表 system_menu
-- --------------------------------------------------

DROP TABLE IF EXISTS `system_menu`;

CREATE TABLE IF NOT EXISTS `system_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  `url` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL DEFAULT '',
  `pid` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

INSERT INTO `system_menu` VALUES('1','系统核心','#','','0','50','1');
INSERT INTO `system_menu` VALUES('2','系统设置','#','','1','50','1');
INSERT INTO `system_menu` VALUES('3','权限节点','admin/node/index','','2','50','1');
INSERT INTO `system_menu` VALUES('4','菜单设置','admin/menu/index','','2','50','1');
INSERT INTO `system_menu` VALUES('9','角色授权','admin/role/index','','2','50','1');
INSERT INTO `system_menu` VALUES('10','后台用户','admin/admin/index','','2','50','1');
INSERT INTO `system_menu` VALUES('11','内容门户','#','','0','50','0');
INSERT INTO `system_menu` VALUES('12','内容管理','#','','11','50','0');
INSERT INTO `system_menu` VALUES('13','分类管理','admin/category/index','','12','50','0');
INSERT INTO `system_menu` VALUES('14','文章管理','admin/article/index','','12','50','0');
INSERT INTO `system_menu` VALUES('15','友情链接','admin/link/index','','11','50','0');
INSERT INTO `system_menu` VALUES('16','广告位管理','#','','11','50','0');
INSERT INTO `system_menu` VALUES('17','广告分类','admin/adscate/index','','16','50','0');
INSERT INTO `system_menu` VALUES('18','广告链接','admin/ads/index','','16','50','0');
INSERT INTO `system_menu` VALUES('19','网站配置','#','','0','49','0');
INSERT INTO `system_menu` VALUES('20','基础配置','admin/config/edit','','19','50','0');
INSERT INTO `system_menu` VALUES('21','标签管理','admin/tags/index','','12','50','0');


-- --------------------------------------------------
--          数据表 system_role
-- --------------------------------------------------

DROP TABLE IF EXISTS `system_role`;

CREATE TABLE IF NOT EXISTS `system_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(20) NOT NULL COMMENT '名称',
  `access_list` varchar(255) NOT NULL DEFAULT '' COMMENT '权限列表',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `system_role` VALUES('2','管理员','53,58,55,54,52,56,57,101,41,44,43,33,37,35,49,47,50,48,46');
INSERT INTO `system_role` VALUES('3','网站编辑','90,93,92,91,89,96,99,98,97,95,72,75,74,73,71,67,77,76,68,66,80,83,82,81,79,105,104');


